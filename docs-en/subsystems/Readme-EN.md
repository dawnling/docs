# Subsystem Development Guidelines

-   [Distributed Remote Startup](distributed-remote-startup.md)
-   [Graphics](graphics.md)
    -   [Graphics](graphics-0.md)
    -   [Development Guidelines on Container Components](development-guidelines-on-container-components.md)
    -   [Development Guidelines on Layout Container Components](development-guidelines-on-layout-container-components.md)
    -   [Development Guidelines on Common Components](development-guidelines-on-common-components.md)
    -   [Development Guidelines on Animators](development-guidelines-on-animators.md)

-   [Multimedia](multimedia.md)
    -   [Overview](overview.md)
    -   [Development Guidelines on Photographing](development-guidelines-on-photographing.md)
    -   [Development Guidelines on Video Recording](development-guidelines-on-video-recording.md)
    -   [Development Guidelines on Previewing](development-guidelines-on-previewing.md)
    -   [Overview](overview-1.md)
    -   [Development Guidelines on Media Playback](development-guidelines-on-media-playback.md)
    -   [Development Guidelines on Media Recording](development-guidelines-on-media-recording.md)

-   [Utils](utils.md)
    -   [Utils Overview](utils-overview.md)
    -   [Utils Development Guidelines](utils-development-guidelines.md)
    -   [Utils FAQ](utils-faq.md)

-   [Application Framework](application-framework.md)
    -   [Overview](overview-2.md)
    -   [Setting Up a Development Environment](setting-up-a-development-environment.md)
    -   [Development Guidelines](development-guidelines.md)
    -   [Development Example](development-example.md)
    -   [FAQ](faq.md)

-   [Compilation and Building](compilation-and-building.md)
    -   [Compilation and Building Overview](compilation-and-building-overview.md)
    -   [Compilation and Building Guidelines](compilation-and-building-guidelines.md)
    -   [Compilation and Building FAQ](compilation-and-building-faq.md)

-   [Testing](testing.md)
-   [DFX](dfx.md)
    -   [DFX](dfx-3.md)
    -   [Development Guidelines on the mini Framework](development-guidelines-on-the-mini-framework.md)
    -   [Development Guidelines on the featured Framework](development-guidelines-on-the-featured-framework.md)
    -   [FAQ](faq-4.md)
    -   [Reference](reference.md)

-   [XTS](xts.md)

