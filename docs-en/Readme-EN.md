# WELCOME TO OPENHARMONY
OpenHarmony is an open-source project launched by the OpenAtom Foundation and serves as an open-source, distributed operating system that is intended to address all conceivable usage scenarios. OpenHarmony is an open-source version of HarmonyOS.
HarmonyOS is a future-proof distributed operating system open to you as part of the initiatives for the all-scenario strategy. It is adaptable to mobile office, fitness and health, social communication, entertainment, and many other scenarios. Unlike any legacy operating system that runs on a standalone device, HarmonyOS is built on a distributed architecture designed based on a set of system capabilities. It is able to run on a wide range of device forms, including smartphones, wearables, tablets, PCs, smart TVs, AI speakers, head units, earphones, and AR/VR glasses.

## Motivation
Traditional OSs are limited to a specific type of device, while OpenHarmony provides distributed features that are compatible with a wide range of different devices. The first version supports devices with 128 KB to 128 MB of memory. Join us as we keep updating OpenHarmony versions.
For device developers, OpenHarmony utilizes a component-based design to tailor its features to better suit specific devices, based on each device's capabilities and service characteristics. OpenHarmony can run on devices with limited resources and wearables with hundreds of KB of memory, as well as more powerful devices, such as smart home cameras and dashcams with hundreds of MB of memory.

[中文版本](/Readme-CN.md)

## OpenHarmony Documentation
- [Getting Started](https://gitee.com/openharmony/docs/blob/master/docs-en/quick-start/Readme-EN.md)
- [Source Code Acquisition/Tool Acquisition](https://gitee.com/openharmony/docs/blob/master/docs-en/get-code/source-code-acquisition.md)
- [Kernel Usage Guidelines](https://gitee.com/openharmony/docs/blob/master/docs-en/kernel/Readme-EN.md)
- [Driver Usage Guidelines](https://gitee.com/openharmony/docs/blob/master/docs-en/driver/Readme-EN.md)
- [Subsystem Development Guidelines](https://gitee.com/openharmony/docs/blob/master/docs-en/subsystems/Readme-EN.md)
- [Bundle Development Guidelines](https://gitee.com/openharmony/docs/blob/master/docs-en/bundles/Readme-EN.md)
- [Device Development Guidelines](https://gitee.com/openharmony/docs/blob/master/docs-en/guide/Readme-EN.md) 
- [Third-Party Library Porting Guidelines](https://gitee.com/openharmony/docs/blob/master/docs-en/porting/Readme-EN.md)
- [Privacy and Security](https://gitee.com/openharmony/docs/blob/master/docs-en/security/Readme-EN.md)
- [LinkIoT Modules API](https://gitee.com/openharmony/docs/blob/master/docs-en/api/api-LinkIoT/Readme-EN.md)
- [SmartVision Devices API](https://gitee.com/openharmony/docs/blob/master/docs-en/api/api-SmartVision-Devices/Readme-EN.md)
- [Glossary](https://gitee.com/openharmony/docs/blob/master/docs-en/term/glossary.md)

## Start Contributing
OpenHarmony is an open-source community and encourages everyone in the community to submit patches directly to the project. You can contribute either to the code or documentation.
- [Contributing to the Code](https://gitee.com/openharmony/docs/blob/master/docs-en/contribute/contributing-code.md)
- [Contributing to the Documentation](https://gitee.com/openharmony/docs/blob/master/docs-en/contribute/documentation-contribution.md)

For more details on how to contribute, see [Contributing Documents](https://gitee.com/openharmony/docs/tree/master/docs-en/contibute).
## Get Source Code and Tools
To download the source code and tools required for the project, click the following links:
- [Source Code Acquisition](https://gitee.com/openharmony/docs/blob/master/docs-en/get-code/source-code-acquisition.md)
- [Tool Acquisition](https://gitee.com/openharmony/docs/blob/master/docs-en/get-code/tool-acquisition.md)

## Where to Turn for Help
You can subscribe to the mailing list to get constant updates on new features, released road maps, and community activities. For how to subscribe to the mailing list or get any other support, see [Communication in Community](https://gitee.com/openharmony/docs/tree/master/docs-en/contribute/communication-in-community.md).

## Code of Conduct
OpenHarmony is committed to providing a welcoming and inspiring community for all. In order to maintain a collaborative environment, we recommend you read and follow the community [Code of Conduct](https://gitee.com/openharmony/docs/blob/master/docs-en/contribute/code-of-conduct.md). 

## Contributors & Acknowledgements
A great open-source project wouldn't be possible without the hard work of many contributors. We'd like to invite anyone from around the world to participate in this exciting journey, and we're grateful for your time, passion, and efforts!

## Release
In order to provide new features and bug fixes, the community constantly updates code and provides new releases.
