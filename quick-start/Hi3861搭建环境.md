# Hi3861搭建环境<a name="ZH-CN_TOPIC_0000001054781998"></a>

-   [环境要求](#section466851916410)
    -   [软件系统要求](#section1595725202314)
    -   [Linux构建工具要求](#section992619473117)
    -   [Windows开发工具要求](#section2833173411317)

-   [安装Linux编译环境（方法一：Docker方式）](#section107932281315)
-   [安装Linux编译环境（方法二：安装包方式）](#section497484245614)
    -   [连接Linux服务器](#section1565413132610)
    -   [将Linux shell改为bash](#section1715027152617)
    -   [安装Python环境](#section18835251192618)
    -   [安装Scons](#section13515123015279)
    -   [安装gn](#section0646125972716)
    -   [安装ninja](#section7681117192813)
    -   [安装gcc\_riscv32（WLAN模组类编译工具链）](#section1842910288284)

-   [安装Windows开发环境](#section1995840162515)

## 环境要求<a name="section466851916410"></a>

### 软件系统要求<a name="section1595725202314"></a>

**表 1**  Hi3861 WLAN模組开发平台要求

<a name="table1822785713303"></a>
<table><thead align="left"><tr id="row72271157163014"><th class="cellrowborder" valign="top" width="12.41%" id="mcps1.2.5.1.1"><p id="p17227205711305"><a name="p17227205711305"></a><a name="p17227205711305"></a>硬件</p>
</th>
<th class="cellrowborder" valign="top" width="12.389999999999999%" id="mcps1.2.5.1.2"><p id="p112271557113019"><a name="p112271557113019"></a><a name="p112271557113019"></a>软件</p>
</th>
<th class="cellrowborder" valign="top" width="46.21%" id="mcps1.2.5.1.3"><p id="p16227105718309"><a name="p16227105718309"></a><a name="p16227105718309"></a>描述</p>
</th>
<th class="cellrowborder" valign="top" width="28.99%" id="mcps1.2.5.1.4"><p id="p162271957163018"><a name="p162271957163018"></a><a name="p162271957163018"></a>备注</p>
</th>
</tr>
</thead>
<tbody><tr id="row16227157163014"><td class="cellrowborder" valign="top" width="12.41%" headers="mcps1.2.5.1.1 "><p id="p1222775733020"><a name="p1222775733020"></a><a name="p1222775733020"></a>Linux编译服务器</p>
</td>
<td class="cellrowborder" valign="top" width="12.389999999999999%" headers="mcps1.2.5.1.2 "><p id="p22272577304"><a name="p22272577304"></a><a name="p22272577304"></a>操作系统</p>
</td>
<td class="cellrowborder" valign="top" width="46.21%" headers="mcps1.2.5.1.3 "><p id="p622719576305"><a name="p622719576305"></a><a name="p622719576305"></a>Ubuntu16.04及以上64位系统版本，Shell使用bash</p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="28.99%" headers="mcps1.2.5.1.4 "><p id="p162276579305"><a name="p162276579305"></a><a name="p162276579305"></a>开发人员可以在Windows工作台中进行程序开发，或者远程登录到Linux服务器进行程序开发。</p>
</td>
</tr>
<tr id="row5227165719305"><td class="cellrowborder" valign="top" headers="mcps1.2.5.1.1 "><p id="p32279578305"><a name="p32279578305"></a><a name="p32279578305"></a>Windows工作台</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.5.1.2 "><p id="p192271757133018"><a name="p192271757133018"></a><a name="p192271757133018"></a>操作系统</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.5.1.3 "><p id="p1022855713303"><a name="p1022855713303"></a><a name="p1022855713303"></a>Windows XP/Windows7/Windows10</p>
</td>
</tr>
</tbody>
</table>

### Linux构建工具要求<a name="section992619473117"></a>

Linux服务器通用环境配置需要的工具及其获取途径如下表所示。

**表 2**  Linux服务器开发工具及获取途径

<a name="table6299192712513"></a>
<table><thead align="left"><tr id="row122993276512"><th class="cellrowborder" valign="top" width="25.562556255625562%" id="mcps1.2.4.1.1"><p id="p1829914271858"><a name="p1829914271858"></a><a name="p1829914271858"></a>开发工具</p>
</th>
<th class="cellrowborder" valign="top" width="20.7020702070207%" id="mcps1.2.4.1.2"><p id="p429918274517"><a name="p429918274517"></a><a name="p429918274517"></a>用途</p>
</th>
<th class="cellrowborder" valign="top" width="53.73537353735374%" id="mcps1.2.4.1.3"><p id="p12997271757"><a name="p12997271757"></a><a name="p12997271757"></a>获取途径</p>
</th>
</tr>
</thead>
<tbody><tr id="row1829915274517"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p1629902717511"><a name="p1629902717511"></a><a name="p1629902717511"></a>交叉编译器gcc_riscv32</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p1219863434519"><a name="p1219863434519"></a><a name="p1219863434519"></a>交叉编译工具</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p14145637348"><a name="p14145637348"></a><a name="p14145637348"></a><a href="https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz" target="_blank" rel="noopener noreferrer">https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz</a></p>
</td>
</tr>
<tr id="row430016273514"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p330015271158"><a name="p330015271158"></a><a name="p330015271158"></a>Python3.7+</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p43003270510"><a name="p43003270510"></a><a name="p43003270510"></a>编译构建工具</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p18254155164617"><a name="p18254155164617"></a><a name="p18254155164617"></a><a href="https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tgz" target="_blank" rel="noopener noreferrer">https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tgz</a></p>
</td>
</tr>
<tr id="row1397335913612"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p097355911620"><a name="p097355911620"></a><a name="p097355911620"></a>SCons3.0.4+</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p1973195917619"><a name="p1973195917619"></a><a name="p1973195917619"></a>编译构建工具</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p1722663441514"><a name="p1722663441514"></a><a name="p1722663441514"></a>通过互联网获取</p>
</td>
</tr>
<tr id="row42668197206"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p426711912014"><a name="p426711912014"></a><a name="p426711912014"></a>bash</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p14267131962014"><a name="p14267131962014"></a><a name="p14267131962014"></a>命令处理器</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p14267101962014"><a name="p14267101962014"></a><a name="p14267101962014"></a>通过互联网获取</p>
</td>
</tr>
<tr id="row1463517494402"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p258814561424"><a name="p258814561424"></a><a name="p258814561424"></a>build-essential</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p1749611716181"><a name="p1749611716181"></a><a name="p1749611716181"></a>编译依赖的基础软件包</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p8635174916403"><a name="p8635174916403"></a><a name="p8635174916403"></a>通过互联网获取</p>
</td>
</tr>
<tr id="row1711946154018"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p15588165684216"><a name="p15588165684216"></a><a name="p15588165684216"></a>gn</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p4588135634213"><a name="p4588135634213"></a><a name="p4588135634213"></a>产生ninja编译脚本</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p10572194318318"><a name="p10572194318318"></a><a name="p10572194318318"></a><a href="https://repo.huaweicloud.com/harmonyos/compiler/gn/1523/linux/gn.1523.tar" target="_blank" rel="noopener noreferrer">https://repo.huaweicloud.com/harmonyos/compiler/gn/1523/linux/gn.1523.tar</a></p>
</td>
</tr>
<tr id="row16990164213404"><td class="cellrowborder" valign="top" width="25.562556255625562%" headers="mcps1.2.4.1.1 "><p id="p1858825613428"><a name="p1858825613428"></a><a name="p1858825613428"></a>ninja</p>
</td>
<td class="cellrowborder" valign="top" width="20.7020702070207%" headers="mcps1.2.4.1.2 "><p id="p15844174611816"><a name="p15844174611816"></a><a name="p15844174611816"></a>执行ninja编译脚本</p>
</td>
<td class="cellrowborder" valign="top" width="53.73537353735374%" headers="mcps1.2.4.1.3 "><p id="p1923373393515"><a name="p1923373393515"></a><a name="p1923373393515"></a><a href="https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar" target="_blank" rel="noopener noreferrer">https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar</a></p>
</td>
</tr>
</tbody>
</table>

### Windows开发工具要求<a name="section2833173411317"></a>

**表 3**  Windows工作台开发工具及获取途径

<a name="table1313323311274"></a>
<table><thead align="left"><tr id="row3133133312711"><th class="cellrowborder" valign="top" width="16.371637163716375%" id="mcps1.2.4.1.1"><p id="p16132203372716"><a name="p16132203372716"></a><a name="p16132203372716"></a>开发工具</p>
</th>
<th class="cellrowborder" valign="top" width="50.29502950295029%" id="mcps1.2.4.1.2"><p id="p1413219339278"><a name="p1413219339278"></a><a name="p1413219339278"></a>用途</p>
</th>
<th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.2.4.1.3"><p id="p17133183312711"><a name="p17133183312711"></a><a name="p17133183312711"></a>获取途径</p>
</th>
</tr>
</thead>
<tbody><tr id="row138432533451"><td class="cellrowborder" valign="top" width="16.371637163716375%" headers="mcps1.2.4.1.1 "><p id="p163612016916"><a name="p163612016916"></a><a name="p163612016916"></a>Visual Studio Code</p>
</td>
<td class="cellrowborder" valign="top" width="50.29502950295029%" headers="mcps1.2.4.1.2 "><p id="p1563690791"><a name="p1563690791"></a><a name="p1563690791"></a>代码编辑工具。</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.3 "><p id="p146361701097"><a name="p146361701097"></a><a name="p146361701097"></a><a href="https://code.visualstudio.com/" target="_blank" rel="noopener noreferrer">https://code.visualstudio.com/</a></p>
</td>
</tr>
<tr id="row12133123315277"><td class="cellrowborder" valign="top" width="16.371637163716375%" headers="mcps1.2.4.1.1 "><p id="p613393317271"><a name="p613393317271"></a><a name="p613393317271"></a>HUAWEI DevEco Device Tool</p>
</td>
<td class="cellrowborder" valign="top" width="50.29502950295029%" headers="mcps1.2.4.1.2 "><p id="p10133193310276"><a name="p10133193310276"></a><a name="p10133193310276"></a>IDE开发工具，支持WLAN模组的代码编写、远程编译、版本烧录、串口调试等功能。</p>
<div class="note" id="note7133193392718"><a name="note7133193392718"></a><a name="note7133193392718"></a><span class="notetitle"> 说明： </span><div class="notebody"><p id="p1133183310277"><a name="p1133183310277"></a><a name="p1133183310277"></a>HUAWEI DevEco Device Tool是<span id="text4133533192719"><a name="text4133533192719"></a><a name="text4133533192719"></a>OpenHarmony</span>面向智能设备开发者提供的一站式集成开发环境，支持<span id="text4133933162719"><a name="text4133933162719"></a><a name="text4133933162719"></a>OpenHarmony</span>的组件按需定制，支持C/C++语言，以插件的形式部署在Visual Studio Code上。</p>
</div></div>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.3 "><p id="p1240954393114"><a name="p1240954393114"></a><a name="p1240954393114"></a><a href="https://device.harmonyos.com/cn/ide" target="_blank" rel="noopener noreferrer">https://device.harmonyos.com/cn/ide</a></p>
</td>
</tr>
<tr id="row18342145821513"><td class="cellrowborder" valign="top" width="16.371637163716375%" headers="mcps1.2.4.1.1 "><p id="p123421958131517"><a name="p123421958131517"></a><a name="p123421958131517"></a>PuTTY或其他超级终端（选其一）</p>
</td>
<td class="cellrowborder" valign="top" width="50.29502950295029%" headers="mcps1.2.4.1.2 "><p id="p18342658131511"><a name="p18342658131511"></a><a name="p18342658131511"></a>远程连接Linux编译服务器，连接模组串口工具。</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.3 "><p id="p17342125851518"><a name="p17342125851518"></a><a name="p17342125851518"></a>通过互联网获取（如：<a href="https://www.putty.org/" target="_blank" rel="noopener noreferrer">https://www.putty.org/</a>）</p>
</td>
</tr>
<tr id="row244884215147"><td class="cellrowborder" valign="top" width="16.371637163716375%" headers="mcps1.2.4.1.1 "><p id="p1044974291416"><a name="p1044974291416"></a><a name="p1044974291416"></a>CH341SER.EXE</p>
</td>
<td class="cellrowborder" valign="top" width="50.29502950295029%" headers="mcps1.2.4.1.2 "><p id="p94491342131413"><a name="p94491342131413"></a><a name="p94491342131413"></a>USB转串口驱动。</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.3 "><p id="p6449184214148"><a name="p6449184214148"></a><a name="p6449184214148"></a><a href="http://www.wch.cn/search?q=ch340g&amp;t=downloads" target="_blank" rel="noopener noreferrer">http://www.wch.cn/search?q=ch340g&amp;t=downloads</a></p>
</td>
</tr>
</tbody>
</table>

## 安装Linux编译环境（方法一：Docker方式）<a name="section107932281315"></a>

Linux编译环境可以通过Docker方式快速安装，具体请参见[Docker方式获取编译环境](../get-code/获取工具.md)的“搭建Docker环境”。

## 安装Linux编译环境（方法二：安装包方式）<a name="section497484245614"></a>

>![](public_sys-resources/icon-notice.gif) **须知：** 
>-   如果后续通过“HPM组件方式”或“HPM包管理器命令行工具方式”获取源码，不需要安装gn、ninja、gcc\_riscv32编译工具。
>-   （推荐）如果后续通过“镜像站点方式”或“代码仓库方式”获取源码，需要安装gn、ninja、gcc\_riscv32编译工具。
>    安装gn、ninja、gcc\_riscv32编译工具时，请确保编译工具的环境变量路径唯一。

### 连接Linux服务器<a name="section1565413132610"></a>

使用Windows远程登录Linux，以PUTTY工具为例：

1.  打开[PuTTY工具](https://www.putty.org/)，输入Linux服务器IP地址，后点击“open”，打开连接。

    **图 1**  PuTTY界面示例图<a name="f825e6d7569cc487db9b744e454c7157a"></a>  
    ![](figures/PuTTY界面示例图.png "PuTTY界面示例图")

2.  弹框告警选择“是\(Y\)”。
3.  弹出界面中输入帐号并按回车键，继续输入密码并回车键。

    **图 2**  登录界面图<a name="fe1428afffbdd45f9b4a39497691ff5ff"></a>  
    ![](figures/登录界面图.png "登录界面图")

4.  登录成功。

    **图 3**  登录成功界面<a name="f11bea69483e14bd39fef4a10b1ed1a3a"></a>  
    ![](figures/登录成功界面.png "登录成功界面")


### 将Linux shell改为bash<a name="section1715027152617"></a>

查看shell是否为bash，在终端运行如下命令

```
ls -l /bin/sh
```

如果显示为“/bin/sh -\> bash”则为正常，否则请按以下方式修改：

**方法一**：在终端运行如下命令，然后选择 no。

```
sudo dpkg-reconfigure dash
```

**方法二**：先删除sh，再创建软链接。

```
sudo rm -rf /bin/sh
sudo ln -s /bin/bash /bin/sh
```

### 安装Python环境<a name="section18835251192618"></a>

1.  打开Linux编译服务器终端。
2.  输入如下命令，查看python版本号，需使用python3.7以上版本。

    ```
    python3 --version
    ```

    如果低于python3.7版本，不建议直接升级，请按照如下步骤重新安装。以python3.8为例，按照以下步骤安装python。

    1.  运行如下命令，查看Ubuntu版本：

    ```
    cat /etc/issue
    ```

    1.  根据Ubuntu不同版本，安装python。
        -   如果Ubuntu 版本为18+，运行如下命令。

            ```
            sudo apt-get install python3.8
            ```

        -   如果Ubuntu版本为16，请以下载包的方式安装python。

            1.  python安装环境依赖\(gcc, g++, make, zlib, libffi\)请运行如下命令：

            ```
            sudo apt-get install gcc && sudo apt-get install g++ && sudo apt-get install make && sudo apt-get install zlib* && sudo apt-get install libffi-dev
            ```

            1.  获取[python3.8.5安装包](https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tgz)，将其放至linux服务器中，运行如下命令 ：

            ```
            tar -xvzf Python-3.8.5.tgz && cd Python-3.8.5 && sudo ./configure && sudo make && sudo make install
            ```



3.  确定python安装好后，将python路径链接到"/usr/bin/python"。

    输入如下命令，查看python是否正确软链接到python3.8。

    ```
    python --version
    ```

    如果回显不是python 3.8.5，则运行如下命令，查看python3.8所在目录：

    ```
    which python3.8
    ```

    将以下命令中的 "python3.8-path" 替换为 "which python3.8" 命令执行后的回显路径

    ```
    cd /usr/bin && sudo rm python && sudo rm python3 && sudo ln -s python3.8-path python && sudo ln -s python3.8-path python3 && python3 --version && python --version
    ```

4.  安装并升级Python包管理工具（pip3），任选如下一种方式。
    -   **命令行方式：**

        ```
        sudo apt-get install python3-setuptools python3-pip -y
        sudo pip3 install --upgrade pip
        ```

    -   **安装包方式：**

        ```
        curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
        python get-pip.py
        ```


5.  运行如下命令，安装python模块setuptools。

    ```
    pip3 install setuptools
    ```

6.  安装GUI menuconfig工具（Kconfiglib），建议安装Kconfiglib 13.2.0+版本，任选如下一种方式。
    -   **命令行方式：**

        ```
        sudo pip3 install kconfiglib
        ```


    -   **安装包方式：**
        1.  下载.whl文件（例如：kconfiglib-13.2.0-py2.py3-none-any.whl）。

            下载路径：“[https://pypi.org/project/kconfiglib\#files](https://pypi.org/project/kconfiglib#files)”


        1.  运行如下命令，安装.whl文件。

            ```
            sudo pip3 install kconfiglib-13.2.0-py2.py3-none-any.whl
            ```



7.  安装pycryptodome，任选如下一种方式。

    安装升级文件签名依赖的Python组件包，包括：pycryptodome、six、ecdsa。安装ecdsa依赖six，请先安装six，再安装ecdsa。

    -   **命令行方式：**

        ```
        sudo pip3 install pycryptodome
        ```

    -   **安装包方式：**
        1.  下载.whl文件（例如：pycryptodome-3.9.9-cp38-cp38-manylinux1\_x86\_64.whl）。

            下载路径：“[https://pypi.org/project/pycryptodome/\#files](https://pypi.org/project/pycryptodome/#files)”。


        1.  运行如下命令，安装.whl文件。

            ```
            sudo pip3 install pycryptodome-3.9.9-cp38-cp38-manylinux1_x86_64.whl
            ```



8.  安装six，任选如下一种方式。
    -   **命令行方式：**

        ```
        sudo pip3 install six --upgrade --ignore-installed six
        ```


    -   **安装包方式：**
        1.  下载.whl文件（例如：six-1.12.0-py2.py3-none-any.whl）。

            下载路径：“[https://pypi.org/project/six/\#files](https://pypi.org/project/six/#files)”


        1.  运行如下命令，安装.whl文件。

            ```
            sudo pip3 install six-1.12.0-py2.py3-none-any.whl
            ```



9.  安装ecdsa，任选如下一种方式。
    -   **命令行方式：**

        ```
        sudo pip3 install ecdsa
        ```

    -   **安装包方式：**
        1.  下载.whl文件（例如：ecdsa-0.14.1-py2.py3-none-any.whl）。

            下载路径：“[https://pypi.org/project/ecdsa/\#files](https://pypi.org/project/ecdsa/#files)”


        1.  运行如下命令，安装.whl文件。

            ```
            sudo pip3 install ecdsa-0.14.1-py2.py3-none-any.whl
            ```




### 安装Scons<a name="section13515123015279"></a>

1.  打开Linux编译服务器终端。
2.  运行如下命令，安装SCons安装包。

    ```
    sudo apt-get install scons -y
    ```

    如果软件源中无法找到安装包，请按以下步骤处理：

    1.  下载源码包（下载路径：“  [https://scons.org/pages/download.html](https://scons.org/pages/download.html)  ”，推荐SCons版本是3.0.4+）。
    2.  解压源码包到任意目录（以scons-3.1.2为例）。

        ```
        tar -xvf scons-3.1.2.tar.gz
        ```

    3.  安装源码包：进入解压目录，运行如下命令：

        ```
        sudo python3 setup.py install
        ```


3.  运行如下命令，查看是否安装成功。如果安装成功，查询结果下图所示。

    ```
    scons -v
    ```

    **图 4**  SCons安装成功界面<a name="fig235815252492"></a>  
    ![](figures/SCons安装成功界面.png "SCons安装成功界面")


### 安装gn<a name="section0646125972716"></a>

1.  打开Linux编译服务器终端。
2.  [下载gn工具](https://repo.huaweicloud.com/harmonyos/compiler/gn/1523/linux/gn.1523.tar)。
3.  解压gn安装包至\~/gn路径下。

    ```
    tar -xvf gn.1523.tar -C ~/
    ```

4.  设置环境变量。

    ```
    vim ~/.bashrc
    ```

    将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

    ```
    export PATH=~/gn:$PATH
    ```

5.  生效环境变量。

    ```
    source ~/.bashrc
    ```


### 安装ninja<a name="section7681117192813"></a>

1.  打开Linux编译服务器终端。
2.  [下载ninja工具](https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar)。
3.  解压ninja安装包至\~/ninja路径下。

    ```
    tar -xvf ninja.1.9.0.tar -C ~/
    ```

4.  设置环境变量。

    ```
    vim ~/.bashrc
    ```

    将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

    ```
    export PATH=~/ninja:$PATH
    ```

5.  生效环境变量。

    ```
    source ~/.bashrc
    ```


### 安装gcc\_riscv32（WLAN模组类编译工具链）<a name="section1842910288284"></a>

>![](public_sys-resources/icon-notice.gif) **须知：** 
>Hi3861平台仅支持使用libgcc运行时库的静态链接，**不建议开发者使用libgcc运行时库的动态链接，会导致商业分发时被GPL V3污染。**

1.  打开Linux编译服务器终端。
2.  环境准备，请安装"gcc, g++, bison, flex, makeinfo"软件，确保工具链能正确编译。

    ```
    sudo apt-get install gcc && sudo apt-get install g++ && sudo apt-get install flex bison && sudo apt-get install texinfo
    ```

3.  下载riscv-gnu-toolchain交叉编译工具链。

    ```
    git clone --recursive https://github.com/riscv/riscv-gnu-toolchain
    ```

4.  打开文件夹riscv-gnu-toolchain，先删除空文件夹，以防止下载newlib，binutils，gcc时冲突。

    ```
    cd riscv-gnu-toolchain && rm -rf riscv-newlib && rm -rf riscv-binutils && rm -rf riscv-gcc
    ```

5.  下载riscv-newlib-3.0.0。

    ```
    git clone -b riscv-newlib-3.0.0 https://github.com/riscv/riscv-newlib.git
    ```

6.  下载riscv-binutils-2.31.1。

    ```
    git clone -b riscv-binutils-2.31.1 https://github.com/riscv/riscv-binutils-gdb.git
    ```

7.  下载riscv-gcc-7.3.0。

    ```
    git clone -b riscv-gcc-7.3.0 https://github.com/riscv/riscv-gcc
    ```

8.  下载riscv-gcc-7.3.0补丁。

    拷贝gcc官方[89411.patch](https://gcc.gnu.org/git/?p=gcc.git;a=patch;h=026216a753ef0a757a9e368a59fa667ea422cf09;hp=2a23a1c39fb33df0277abd4486a3da64ae5e62c2)，拷贝gcc官方[86724.patch](https://gcc.gnu.org/git/?p=gcc.git;a=blobdiff;f=gcc/graphite.h;h=be0a22b38942850d88feb159603bb846a8607539;hp=4e0e58c60ab83f1b8acf576e83330466775fac17;hb=b1761565882ed6a171136c2c89e597bc4dd5b6bf;hpb=fbd5f023a03f9f60c6ae36133703af5a711842a3)，并对应代码做一定的行数号修改，将两个patch放入 riscv-gcc目录

    ```
    cd riscv-gcc && patch -p1 < 89411.patch && patch -p1 < 86724.patch
    ```

    patch过程中如遇到Hunk \#1 FAILED等问题，请对应到相应.c文件，修改patch代码所在行数

9.  下载[GMP 6.1.2](https://gmplib.org/download/gmp/gmp-6.1.2.tar.bz2)，并解压安装。

    ```
    tar -xvf mpfr-4.0.2.tar.gz && mkdir build_mpfr && cd build_mpfr && ../mpfr-4.0.2/configure --prefix=/usr/local/mpfr-4.0.2 --with-gmp=/usr/local/gmp-6.1.2 --disable-shared && make && make install
    ```

10. 下载[mpfr-4.0.2 ](https://www.mpfr.org/mpfr-4.0.2/mpfr-4.0.2.tar.gz)，并解压安装。

    ```
    tar -xvf mpfr-4.0.2.tar.gz && mkdir build_mpfr && cd build_mpfr && ../mpfr-4.0.2/configure --prefix=/usr/local/mpfr-4.0.2 --with-gmp=/usr/local/gmp-6.1.2 --disable-shared && make && make install
    ```

11. 下载[mpc-1.1.0](https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz)  ，并解压安装。

    ```
    tar -xvf mpc-1.1.0.tar.gz && mkdir build_mpc && cd build_mpc && ../mpc-1.1.0/configure --prefix=/usr/local/mpc-1.1.0 --with-gmp=/usr/local/gmp-6.1.2 --with-mpfr=/usr/local/mpfr-4.0.2 --disable-shared && make && make install
    ```

12. 打开文件夹riscv-gnu-toolchain，新建工具链输出目录。

    ```
    cd /opt && mkdir gcc_riscv32
    ```

13. 编译bintutils。

    ```
    mkdir build_binutils && cd build_binutils && ../riscv-binutils-gdb/configure --prefix=/opt/gcc_riscv32 --target=riscv32-unknown-elf --with-arch=rv32imc --with-abi=ilp32 --disable-__cxa_atexit --disable-libgomp --disable-libmudflap --enable-libssp --disable-libstdcxx-pch --disable-nls --disable-shared --disable-threads --disable-multilib --enable-poison-system-directories --enable-languages=c,c++ --with-gnu-as --with-gnu-ld --with-newlib --with-system-zlib CFLAGS="-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -fPIE" CXXFLAGS="-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -fPIE" CXXFLAGS_FOR_TARGET="-Os -mcmodel=medlow -Wall -fstack-protector-strong -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -fno-short-enums -fno-short-wchar" CFLAGS_FOR_TARGET="-Os -mcmodel=medlow -Wall -fstack-protector-strong -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -fno-short-enums -fno-short-wchar" --bindir=/opt/gcc_riscv32/bin --libexecdir=/opt/gcc_riscv32/riscv32 --libdir=/opt/gcc_riscv32 --includedir=/opt/gcc_riscv32 && make -j16 && make install
    ```

14. 编译newlib。

    ```
    mkdir build_newlib && cd build_newlib && ../riscv-newlib/configure --prefix=/opt/gcc_riscv32 --target=riscv32-unknown-elf --with-arch=rv32imc --with-abi=ilp32 --disable-__cxa_atexit --disable-libgomp --disable-libmudflap --enable-libssp --disable-libstdcxx-pch --disable-nls --disable-shared --disable-threads --disable-multilib --enable-poison-system-directories --enable-languages=c,c++ --with-gnu-as --with-gnu-ld --with-newlib --with-system-zlib CFLAGS="-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -fPIE" CXXFLAGS="-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -fPIE" \CXXFLAGS_FOR_TARGET="-Os -mcmodel=medlow -Wall -fstack-protector-strong -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -fno-short-enums -fno-short-wchar" CFLAGS_FOR_TARGET="-Os -mcmodel=medlow -Wall -fstack-protector-strong -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -fno-short-enums -fno-short-wchar" --bindir=/opt/gcc_riscv32/bin --libexecdir=/opt/gcc_riscv32 --libdir=/opt/gcc_riscv32 --includedir=/opt/gcc_riscv32 && make -j16 && make install
    ```

15. 编译gcc。

    ```
    mkdir build_gcc && cd build_gcc && ../riscv-gcc/configure --prefix=/opt/gcc_riscv32 --target=riscv32-unknown-elf --with-arch=rv32imc --with-abi=ilp32 --disable-__cxa_atexit --disable-libgomp --disable-libmudflap --enable-libssp --disable-libstdcxx-pch --disable-nls --disable-shared --disable-threads --disable-multilib --enable-poison-system-directories --enable-languages=c,c++ --with-gnu-as --with-gnu-ld --with-newlib --with-system-zlib CFLAGS="-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -fPIE" CXXFLAGS="-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -fPIE" LDFLAGS="-Wl,-z,relro,-z,now,-z,noexecstack" CXXFLAGS_FOR_TARGET="-Os -mcmodel=medlow -Wall -fstack-protector-strong -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -fno-short-enums -fno-short-wchar" CFLAGS_FOR_TARGET="-Os -mcmodel=medlow -Wall -fstack-protector-strong -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -fno-short-enums -fno-short-wchar" --with-headers="/opt/gcc-riscv32/riscv32-unknown-elf/include" --with-mpc=/usr/local/mpc-1.1.0 --with-gmp=/usr/local/gmp-6.1.2 --with-mpfr=/usr/local/mpfr-4.0.2 && make -j16 && make install
    ```

16. 设置环境变量。

    ```
    vim ~/.bashrc
    ```

    将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

    ```
    export PATH=/opt/gcc_riscv32/bin:$PATH
    ```

17. 生效环境变量。

    ```
    source ~/.bashrc
    ```

18. Shell命令行中输入如下命令，如果能正确显示编译器版本号，表明编译器安装成功。

    ```
    riscv32-unknown-elf-gcc -v
    ```


## 安装Windows开发环境<a name="section1995840162515"></a>

OpenHarmony IDE工具DevEco Device Tool集成了终端工具的能力，工具的安装步骤请参考[DevEco Device Tool环境搭建](https://device.harmonyos.com/cn/docs/ide/user-guides/tool_install-0000001050164976)、[导入和配置工程](https://device.harmonyos.com/cn/docs/ide/user-guides/import_project-0000001050164980)完成。

